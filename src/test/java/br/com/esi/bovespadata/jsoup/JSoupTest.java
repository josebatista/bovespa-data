package br.com.esi.bovespadata.jsoup;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.Test;

import us.codecraft.xsoup.Xsoup;

public class JSoupTest {

	
	@Test
	public void testXpath () {
		String HTMLSTring = "<!DOCTYPE html>" + "<html>" + "<head>" + "<title>JSoup Example</title>" + "</head>" + "<body>" + "<table><tr><td><h1 class=\"nome_ativo teste\">HelloWorld1</h1></tr>" + "</table>" + "<table><tr><td><h1 class=\"nome_ativo teste\">HelloWorld2</h1></tr>" + "</table>" + "</body>" + "</html>";

		Document html = Jsoup.parse(HTMLSTring);
		Elements elements= Xsoup.compile("//table").evaluate(html).getElements();
		for (Element element : elements) {
			Elements values=element.getElementsByAttributeValueMatching("class", "nome_ativo");
			System.out.println(values.get(0).text());
		}
	}
}
