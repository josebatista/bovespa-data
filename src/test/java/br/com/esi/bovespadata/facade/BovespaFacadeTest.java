package br.com.esi.bovespadata.facade;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.util.List;

import org.junit.Test;

import br.com.esi.bovespadata.pojo.Acao;

public class BovespaFacadeTest {

	@Test
	public void test () {
		BovespaFacade bovespa = new BovespaFacade();
		List<Acao> acoes = bovespa.getAcoes();
		for (Acao acao:acoes) {
			System.out.println(String.format("Nome: %s  - Sigla: %s  - Valor: %.2f - Percentual: %.2f", 
					acao.getDescricao(),
					acao.getSigla(),
					acao.getValorAtual(),
					acao.getPercAtual()));
		}
	}
	
	@Test
	public void testSerie () {
		BovespaFacade bovespa = new BovespaFacade();
		System.out.println(bovespa.getSerie("BBAS3", 1));
	}
	
	
	@Test
	public void testeData () throws Exception {
		   String dbUrl = "jdbc:postgresql://ec2-50-16-202-213.compute-1.amazonaws.com:5432/dem4cn5fv5pk51?user=trtqzvodygbnbn&password=67831650429dd6f1c2804b0b1b66e0f06923d073252f2db5ac01a68ad4a2097c&sslmode=require";
		   Connection con =  DriverManager.getConnection(dbUrl);
		   con.setAutoCommit(true);
		   con.prepareStatement("CREATE TABLE IF NOT EXISTS USER_TESTE (id SERIAL ,name VARCHAR(20),  PRIMARY KEY(id))").execute();
		   con.prepareStatement("INSERT INTO USER_TESTE (name) VALUES ('teste2')").execute();
		   ResultSet rs = con.prepareStatement("select * from USER_TESTE").executeQuery();
		   rs.next();
		   rs.getString(2);
		   
	}
}
