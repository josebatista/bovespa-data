package br.com.esi.bovespadata.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import br.com.esi.bovespadata.model.Portifolio;
import br.com.esi.bovespadata.service.PortifolioService;

@Controller
public class PortifolioController {
	
	@Autowired
	private PortifolioService portifolioService;

	@GetMapping ("user/{id}/portifolios")
	private ResponseEntity<List<Portifolio>> getAllPortifolioByUser (@PathVariable(name="id") Integer userId) {
		try {
			List <Portifolio> portifolios = portifolioService.getPortifolios(userId);
			if (portifolios.size()>0) {
				return new ResponseEntity<List<Portifolio>> (portifolios,HttpStatus.OK); 
			}else return new ResponseEntity<List<Portifolio>> (HttpStatus.NOT_FOUND);
		}
		catch (Exception e) {
			return new ResponseEntity<List<Portifolio>> (HttpStatus.BAD_REQUEST);
		}
	}
}
