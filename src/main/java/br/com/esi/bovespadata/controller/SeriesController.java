package br.com.esi.bovespadata.controller;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.esi.bovespadata.facade.BovespaFacade;
import br.com.esi.bovespadata.pojo.Serie;

@RestController
public class SeriesController {

	@RequestMapping (value  = "/serie" ,method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
	public Serie getSerie (@RequestParam (required = true) String share, @RequestParam (required = true) int period) {
		return new BovespaFacade().getSerie(share, period);
	}
}
