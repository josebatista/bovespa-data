package br.com.esi.bovespadata.controller;

import java.util.List;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.esi.bovespadata.facade.BovespaFacade;
import br.com.esi.bovespadata.pojo.Acao;

@RestController
public class ShareController {

	@RequestMapping ("/allShares")
	public List <Acao> getSharesList () {
		return new BovespaFacade().getAcoes();
	}
	
	@RequestMapping ("/shares")
	public List <Acao> getSharesList (@RequestParam(required=true)String ...filter ) {		
		return new BovespaFacade().getAcoes(filter);
	}
}
