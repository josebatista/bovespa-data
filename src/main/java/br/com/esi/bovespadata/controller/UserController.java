package br.com.esi.bovespadata.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.util.UriComponentsBuilder;

import br.com.esi.bovespadata.model.User;
import br.com.esi.bovespadata.service.UserService;

@Controller
public class UserController {

	@Autowired
	private UserService userService;
	
	@PostMapping("user")
	private ResponseEntity <User> addUser (@RequestBody User user,UriComponentsBuilder builder) {
		User newUser = userService.addUser(user);
		return new ResponseEntity<User>(newUser,HttpStatus.OK);
	}
	
	@GetMapping("users")
	private ResponseEntity<List<User>> getAllUsers () {
		return new ResponseEntity<List<User>>(userService.getAllUsers(),HttpStatus.OK);
	}
	
	@DeleteMapping ("user/{id}")
	private ResponseEntity <User> deleteUser (@PathVariable(name="id") Integer id) {
		try {
			User deleteUser = this.userService.deleteUser(id);
			return new ResponseEntity<User>(deleteUser,HttpStatus.OK);
		}
		catch (Exception e) {
			return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
		}		
	}
	
	@GetMapping ("user/{id}")
	private ResponseEntity<User> getUserById (@PathVariable(name="id") Integer id) {
		try {
			User user = this.userService.getUserById(id);
			return new ResponseEntity<User>(user,HttpStatus.OK);
		}
		catch (Exception e) {
			return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
		}	
	}
}
