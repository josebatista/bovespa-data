package br.com.esi.bovespadata.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.esi.bovespadata.model.Portifolio;
import br.com.esi.bovespadata.model.User;

@Transactional
@Repository
public class PortifolioDao {

	@PersistenceContext
	private EntityManager entityManager;
	
	
	@SuppressWarnings("unchecked")
	public List<Portifolio> getPortifolios (User user) {
		String hql = "FROM Portifolio as x where x.userId = ?";
		List<Portifolio> result = (List<Portifolio>)this.entityManager.createQuery(hql).setParameter(1, user.getId()).getResultList();
		return result;
	}
}
