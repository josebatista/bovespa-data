package br.com.esi.bovespadata.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.esi.bovespadata.model.User;

@Transactional
@Repository
public class UserDao {
	
	@PersistenceContext
	private EntityManager entityManager;
	
	
	public User addUser (User user){
		entityManager.persist(user);
		entityManager.flush();
		return user;
	}
	
	@SuppressWarnings("unchecked")
	public List <User> getAllUsers () {
		String hql = "SELECT x FROM User x";
		return (List<User>) entityManager.createQuery(hql).getResultList();
	}
	
	public User deleteUser (int userId) {
		User user = getUserById(userId);
		this.entityManager.remove(user);
		return user;
	}
	
	
	public User getUserById (int userId) {
		return this.entityManager.find(User.class, userId);
	}

}
