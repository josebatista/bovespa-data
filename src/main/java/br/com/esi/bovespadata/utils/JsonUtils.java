package br.com.esi.bovespadata.utils;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.JsonArray;

public class JsonUtils {

	
	
	public static List <String> getAsString (JsonArray array) {
		List <String> list = new ArrayList <String>();
		for (int i=0; i< array.size(); i++) {
			list.add(array.get(i).getAsString());
		}
		return list;
	}
}
