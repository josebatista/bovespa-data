package br.com.esi.bovespadata.utils;

public class HtmlUtils {

	
	public static String removeScapeCharacters (String html) {
		return html
				.replaceAll("\\\"", "\"")
				.replaceAll("\\/", "/")
				.replaceAll("\\n","");
	}
}
