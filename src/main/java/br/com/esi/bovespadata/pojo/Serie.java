package br.com.esi.bovespadata.pojo;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Serie {
	
	
	@XmlElement	private String share;
	@XmlElement private Integer period;
	@XmlElementWrapper private List <String> dates;
	@XmlElementWrapper private List <String> values;
	
	public Serie () {
		
	}
	
	public Serie (String share, Integer period,  List <String> dates, List <String> values ) {
		this.share = share;
		this.period = period;
		this.dates = dates;
		this.values = values;
		
	}
	
	
	public String getShare() {
		return share;
	}

	public Integer getPeriod() {
		return period;
	}


	public List<String> getDates() {
		return dates;
	}

	public List<String> getValues() {
		return values;
	}
	
	

}
