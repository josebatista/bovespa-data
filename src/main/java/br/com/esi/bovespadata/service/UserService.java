package br.com.esi.bovespadata.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.esi.bovespadata.dao.UserDao;
import br.com.esi.bovespadata.model.User;

@Service
public class UserService {

	@Autowired
	private UserDao userDao;
	
	public List<User> getAllUsers () {
		return userDao.getAllUsers();
	}
	
	public User addUser (User user) {
		return userDao.addUser(user);
	}
	
	public User deleteUser (int userId) {
		return userDao.deleteUser(userId);
	}
	
	
	public User getUserById (int userId) {
		return userDao.getUserById(userId);
	}
}
