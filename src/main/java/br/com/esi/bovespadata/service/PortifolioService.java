package br.com.esi.bovespadata.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.esi.bovespadata.dao.PortifolioDao;
import br.com.esi.bovespadata.dao.UserDao;
import br.com.esi.bovespadata.model.Portifolio;
import br.com.esi.bovespadata.model.User;

@Service
public class PortifolioService {

	@Autowired
	private PortifolioDao portifolioDao;
	
	@Autowired
	private UserDao userDao;
	
	public List<Portifolio> getPortifolios (int userId) {
		User user = userDao.getUserById(userId);
		return portifolioDao.getPortifolios(user);
	}
	
	public List<Portifolio> getPortifolios (User user, String type) {
		return null;
	}
	
	public Portifolio getPortifolio (Integer id) {
		return null;
	}
	
	public Portifolio deletePortifolio (Portifolio portifolio) {
		return null;
	}
	
	public Portifolio updatePortifolio (Portifolio portifolio) {
		return null;
	}
}
